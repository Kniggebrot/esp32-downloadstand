#include <stdio.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"

// WLAN-HEADER
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "lwip/err.h"
#include "lwip/sys.h"

// SPIFFS
#include "esp_err.h"
#include "esp_spiffs.h"

#include "curl/curl.h"

#include "Mail.h"
#include "WLAN.h"

#undef USEAGB

static const char* TAG = "DS MAIN";

void delay(unsigned int time, int silence) //delay function used for waiting for next mail check
{
	if (silence == 0)
	{
		unsigned int running;
		for (running = 0; running < time; running++)
		{
			ESP_LOGV(TAG, "Noch %u Sekunden...\n", time - running);
			vTaskDelay(1000 / portTICK_PERIOD_MS);
		}
	}
	else
	{
		vTaskDelay(time * 1000 / portTICK_PERIOD_MS);
	}
	return;
}//end delay


// WLAN

static EventGroupHandle_t s_wifi_event_group;
static int s_retry_num = 0;

static void event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data) // Event handler for Wifi-connection(-events)
{
	if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) 
	{
		esp_wifi_connect();
	}
	else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) 
	{
		if (s_retry_num < 10) 
		{
			esp_wifi_connect();
			s_retry_num++;
			ESP_LOGI(TAG, "retry to connect to the AP");
		}
		else 
		{
			xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
		}
		ESP_LOGI(TAG, "Connection to the AP failed");
	}
	else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) 
	{
		ip_event_got_ip_t* event = (ip_event_got_ip_t*)event_data;
		ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
		s_retry_num = 0;
		xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
	}
}

void wifi_init_sta(void)
{
	s_wifi_event_group = xEventGroupCreate();

	tcpip_adapter_init();

	ESP_ERROR_CHECK(esp_event_loop_create_default());
//	esp_netif_create_default_wifi_sta();

	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler, NULL));

	wifi_config_t wifi_config = {
		.sta = {
			.ssid = SSID,
			.password = PASS_WLAN,
		},
	};
	ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
	ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
	ESP_ERROR_CHECK(esp_wifi_start());

	ESP_LOGI(TAG, "wifi_init_sta finished.");

	/* Waiting until either the connection is established (WIFI_CONNECTED_BIT) or connection failed for the maximum
	 * number of re-tries (WIFI_FAIL_BIT). The bits are set by event_handler() (see above) */
	EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
		WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
		pdFALSE,
		pdFALSE,
		portMAX_DELAY);

	/* xEventGroupWaitBits() returns the bits before the call returned, hence we can test which event actually
	 * happened. */
	if (bits & WIFI_CONNECTED_BIT) {
		ESP_LOGI(TAG, "connected to ap SSID:%s",
			SSID);
	}
	else if (bits & WIFI_FAIL_BIT) {
		ESP_LOGI(TAG, "Failed to connect to SSID:%s",
			SSID);
	}
	else {
		ESP_LOGE(TAG, "UNEXPECTED EVENT");
	}

	/* The event will not be processed after unregister */
	ESP_ERROR_CHECK(esp_event_handler_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler));
	ESP_ERROR_CHECK(esp_event_handler_unregister(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler));
	vEventGroupDelete(s_wifi_event_group);
}

// SPIFFS

void spiffs_init()
{
	ESP_LOGI(TAG, "Initializing SPIFFS");

	esp_vfs_spiffs_conf_t conf = {
	  .base_path = "/spiffs",
	  .partition_label = NULL,
	  .max_files = 5,
	  .format_if_mount_failed = true
	};

	// Use settings defined above to initialize and mount SPIFFS filesystem.
	// Note: esp_vfs_spiffs_register is an all-in-one convenience function.
	esp_err_t ret = esp_vfs_spiffs_register(&conf);

	if (ret != ESP_OK) {
		if (ret == ESP_FAIL) {
			ESP_LOGE(TAG, "Failed to mount or format filesystem");
		}
		else if (ret == ESP_ERR_NOT_FOUND) {
			ESP_LOGE(TAG, "Failed to find SPIFFS partition");
		}
		else {
			ESP_LOGE(TAG, "Failed to initialize SPIFFS (%s)", esp_err_to_name(ret));
		}
		return;
	}
}

esp_err_t spiffs_test()
{
	FILE *TestPart = NULL;
	char test[10];
	char test2[12];

	TestPart = fopen("/spiffs/Test.txt", "w+");
	if (TestPart == NULL) 
	{ 
		ESP_LOGE(TAG, "Opening Test.txt failed"); 
		return ESP_FAIL; 
	}

	fprintf(TestPart, "Hallo Welt!\n");
	rewind(TestPart);

	fgets(test2, 12, TestPart);
	char* pos = strchr(test2, '\n');
	if (pos) 
	{
		*pos = '\0';
	}

	ESP_LOGI(TAG, "Test 1: '%s' \n", test2);
	fclose(TestPart);

	TestPart = fopen("/spiffs/last.txt", "r");
	if (TestPart == NULL) 
	{ 
		ESP_LOGE(TAG, "Opening last.txt failed"); 
		return ESP_FAIL; 
	}

	fseek(TestPart, 185L, SEEK_SET);
	fgets(test, 7, TestPart);
	pos = strchr(test, 32);
	if (pos) {
		*pos = '\0';
	}
	ESP_LOGI(TAG, "Test 2: '%s' \n", test);
	fclose(TestPart);
	return ESP_OK;
}

// MAIL-ABFRAGE

void Downloadstand(void)
{
	char sender[] = SENDER;
	char sender_email[] = SENDER_EMAIL;
	char recipient[] = RECIPIENT;
	char recipient_email[] = RECIPIENT_EMAIL;

	int mail_send_ok = CURLE_OK;
	int mail_receive_ok = CURLE_OK;
	int mail_delete_ok = CURLE_OK;

	int DownloadstandNr;
	char Downloadstand[8];

	FILE *lastmail = NULL;

	{
		ESP_LOGI(TAG, "Sende Mail...\n");
		mail_send_ok = SendMail(sender, sender_email, recipient, recipient_email, "/spiffs/DS.txt");

		if (mail_send_ok != CURLE_OK)
		{ 
			ESP_LOGE(TAG, "Fehler beim Senden der Mail...\n");
			esp_restart(); 
		}
		ESP_LOGI(TAG, "Mail gesendet! Warte auf Antwort...\n");
		delay(180, 0);  

		mail_receive_ok = ReceiveMail("/inbox/Downloadstand/");
		if (mail_receive_ok != CURLE_OK)
		{
			esp_restart();
		}
		delay(5,0);

		ESP_LOGI(TAG, "Loesche alte Mail...\n");
		mail_delete_ok = DeleteMail("/inbox/Downloadstand/");
		if (mail_delete_ok != CURLE_OK)
		{
			esp_restart();
		}
		delay(10,0);

		//Jetzt durchsuchen wir die letzte Mail nach dem Downloadstand.
		lastmail = fopen("/spiffs/last.txt", "r");
		//Die ersten 180 Zeichen Text �berspringen wir, um direkt zu der Zahl zu kommen.
		fseek(lastmail, 185, SEEK_SET);
		fread(Downloadstand, sizeof(char), 7, lastmail);
		char * end = strchr(Downloadstand, 32);
		if (end != NULL) *end = '\0';
		ESP_LOGW(TAG, "\n\nDer Downloadstand betraegt: %s MB.\n\n", Downloadstand);
		sscanf(Downloadstand, "%d", &DownloadstandNr);
		fclose(lastmail);
		delay(1800, 1);
	}

}//end Downloadstand


void app_main()
{
    ESP_LOGI(TAG,"Willkommen im Downloadstand-Programm!\n");
	TaskHandle_t Downloadstand_Handle = NULL;

	// WLAN
	//Initialize NVS for WLAN

	esp_err_t ret = nvs_flash_init();
	if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
		ESP_ERROR_CHECK(nvs_flash_erase());
		ret = nvs_flash_init();
	}
	ESP_ERROR_CHECK(ret);

	wifi_init_sta();

	// SPIFFS

	spiffs_init();
	ESP_ERROR_CHECK(spiffs_test());

	// MAIL

	xTaskCreatePinnedToCore((TaskFunction_t)&Downloadstand, "Downloadstand", 8192, NULL, 4, Downloadstand_Handle, 0);
	
	vTaskDelay(10000 / portTICK_PERIOD_MS);
	//esp_restart();
}
