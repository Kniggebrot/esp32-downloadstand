#ifndef HAVE_MAIL_H
#define HAVE_MAIL_H

#include "curl/curl.h"
#include "esp_log.h"

#define USERNAME "dummy@dummymail.com"
#define PASSWORD "dummypass"

#define SMTP_SERVER "smtp://dummyserver.com:587"
#define IMAP_SERVER "imap://dummyserver.com"
#define CACERT "/spiffs/cacert.pem"

#define SENDER "Dummy Person"
#define SENDER_EMAIL "dummy@dummymail.com"
#define RECIPIENT "Downloadstand"
#define RECIPIENT_EMAIL "downloadstand@dummymail.com"

extern int SendMail(char *sender,char *sender_email, char *recipient, char *recipient_mail, const char *mailtxt);
extern int ReceiveMail(const char *Directory);
extern int DeleteMail(const char *Directory);
extern void GPIO_Voltage(int volume);

#endif // HAVE_MAIL_H
