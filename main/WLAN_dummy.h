#ifndef HAVE_WLAN_H
#define HAVE_WLAN_H
/** WLAN-Header **/

#define SSID "dummyssid"
#define PASS_WLAN "dummypassword"

// ESP-spezifisch
#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT      BIT1

#endif // HAVE_WLAN_H
