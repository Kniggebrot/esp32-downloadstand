#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Mail.h"

static char TAG[]="DS RECEIVEMAIL";

int ReceiveMail(const char *Directory)
{
    char * address;

    //F�ge Ordner und Server zusammen
    address=(char *)calloc(75,sizeof(char));
    strcat(address, IMAP_SERVER);
    strcat(address, Directory);
    strcat(address, ";UID=2/;SECTION=TEXT");

    FILE * Mailtxt=NULL;

    CURL* curl;
    CURLcode res=CURLE_OK;

    Mailtxt=fopen("/spiffs/last.txt","w");
    if (Mailtxt==NULL){printf("Fehler beim Schreiben der Maildatei...\n");return 42;}

    curl=curl_easy_init();
    if(curl)
    {
        curl_easy_setopt(curl, CURLOPT_USERNAME, USERNAME);
        curl_easy_setopt(curl, CURLOPT_PASSWORD, PASSWORD);

        curl_easy_setopt(curl, CURLOPT_URL, address);

        curl_easy_setopt(curl, CURLOPT_USE_SSL, (long)CURLUSESSL_ALL);
        curl_easy_setopt(curl, CURLOPT_CAINFO, CACERT);

        curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)Mailtxt);

        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

        /*And here we go*/
        res = curl_easy_perform(curl);

    if(res != CURLE_OK)
      ESP_LOGE(TAG, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));

    /* Always cleanup */
    curl_easy_cleanup(curl);
    }
    fclose(Mailtxt);

    return (int)res;
}

int DeleteMail(const char *Directory)
{
    CURL *curl;
    CURLcode res=CURLE_OK;
    char * address;

    //F�ge Ordner und Server zusammen
    address=(char *)calloc(75,sizeof(char));
    strcat(address, IMAP_SERVER);
    strcat(address, Directory);

    curl=curl_easy_init();
    if(curl)
    {
        curl_easy_setopt(curl, CURLOPT_USERNAME, USERNAME);
        curl_easy_setopt(curl, CURLOPT_PASSWORD, PASSWORD);

        curl_easy_setopt(curl, CURLOPT_URL, address);
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "STORE 1 +Flags \\Deleted");

        curl_easy_setopt(curl, CURLOPT_USE_SSL, (long)CURLUSESSL_ALL);
        curl_easy_setopt(curl, CURLOPT_CAINFO, CACERT);

        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

        /*And here we go*/
        res = curl_easy_perform(curl);

    if(res != CURLE_OK)
      ESP_LOGE(TAG, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));

    /* Always cleanup */
    curl_easy_cleanup(curl);
    }

    return (int)res;
}
