# Downloadstand (latest ESP-IDF)

Residents of the Studierendenwerk Hamburg can access the amount of their used data volume by sending a mail to the downloadstand service of the Studierendenwerk Hamburg. This program for the ESP32 automates this process by sending a mail every 30 minutes, extracting the downloaded amount of data from the response and outputting it on its log, which can be viewed via a serial connection and [PuTTY](https://www.putty.org/) or the built-in `idf.py monitor` tool.

## Installation

This project uses the latest stable ESP-IDF [v4.0.1](https://github.com/espressif/esp-idf/releases/tag/v4.0.1) at the time of writing.

First, rename "Mail_dummy.h" and "WLAN_dummy.h" to "Mail.h" and "WLAN.h" and provide the details for your Wifi and mail account. After this, make sure the custom partions table "partitions.csv" is used with the standard offset and flash the project to your ESP.

The SPIFFS image will be generated and flashed on build/flash based on the contents of [main/spiffs](https://gitlab.com/Kniggebrot/esp32-downloadstand/-/tree/master/main/spiffs). Please make sure you provide a sufficient, yet not too large certificate file for your mail server as "cacert.pem"; mbedtls can't load too large certificates. If you want to flash a custom image, you can use "flash_stuff.sh"; for configuration of its parameters and creation of a SPIFFS image please refer to the ESP [documentation](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/storage/spiffs.html).

Now you are done! You can monitor your ESP using the method of your choice.
